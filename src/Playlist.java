import javafx.scene.control.TableRow;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableRowSorter;
import java.awt.*;
import java.awt.event.*;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.*;
import java.io.*;

public class Playlist {
    final static int MIN_WIDTH = 135;
    final static String SAVED = "Songs saved as ";
    final static String SUCCESS = "Success";

    public void startup(){

        CustomTableModel t = new CustomTableModel(0,5);
        JTable table = new JTable(t);
        JPanel panel = new JPanel(new BorderLayout());
        JPanel opButtonsPanel = new JPanel();
        JPanel filterButtonsPanel = new JPanel(new GridLayout(2,5));
        JPanel sliderPanel = new JPanel();
        JPanel buttonsPanel = new JPanel();
        table.setAutoCreateRowSorter(true);



        JButton bGetMinArtis = new JButton("Min Artist");
        JButton bGetMaxArtis = new JButton("Max Artist");
        JButton bGetMaxTitle = new JButton("Max Title");
        JButton bGetMinTitle = new JButton("Min Title");
        JButton bGetMaxGenre = new JButton("Max Genre");
        JButton bGetMinGenre = new JButton("Min Genre");
        JButton bGetMaxDuration = new JButton("Max Duration");
        JButton bGetMinDuration = new JButton("Min Duration");
        JButton bGetMaxDate = new JButton("Max Date");
        JButton bGetMinDate = new JButton("Min Date");

        JButton bAdd = new JButton("Delete filters");
        JButton bLoad = new JButton("Load");
        JButton addNew = new JButton("Add");
        JButton bSave = new JButton("Save");
        JButton bDelete = new JButton("Delete");
        JButton bSearch = new JButton("Search");

        Vector<String> parameters = new Vector<String>();
        parameters.add("wszędzie");
        for(int i = 0;i<t.getColumnCount();i++) {
            parameters.add(t.getColumnName(i));
        }
        JComboBox<String> choseParameter = new JComboBox<>(parameters);

        JTextField textField = new JTextField(10);
        textField.setPreferredSize( new Dimension( 200, 24 ) );
        textField.setVisible(true);
        table.setSelectionBackground(Color.GREEN);

        JSlider s0 = new JSlider();
        s0.setPaintLabels(true);
        s0.setPaintTicks(true);
        s0.setMajorTickSpacing(60);
        s0.setMinorTickSpacing(10);

        JSlider s1 = new JSlider();
        s1.setPaintLabels(true);
        s1.setPaintTicks(true);
        s1.setMajorTickSpacing(60);
        s1.setMinorTickSpacing(10);
        JPanel searchPanel = new JPanel();

        sliderPanel.add(s0);
        sliderPanel.add(s1);
        searchPanel.add(textField);
        searchPanel.add(choseParameter);
        searchPanel.add(bSearch);
        searchPanel.add(sliderPanel);
        panel.add(searchPanel,BorderLayout.NORTH);
        opButtonsPanel.add(addNew);
        opButtonsPanel.add(bDelete);
        opButtonsPanel.add(bAdd);
        opButtonsPanel.add(bLoad);
        opButtonsPanel.add(bSave);

        filterButtonsPanel.add(bGetMaxArtis);
        filterButtonsPanel.add(bGetMinArtis);
        filterButtonsPanel.add(bGetMaxTitle);
        filterButtonsPanel.add(bGetMinTitle);
        filterButtonsPanel.add(bGetMaxGenre);
        filterButtonsPanel.add(bGetMinGenre);
        filterButtonsPanel.add(bGetMaxDate);
        filterButtonsPanel.add(bGetMinDate);
        filterButtonsPanel.add(bGetMaxDuration);
        filterButtonsPanel.add(bGetMinDuration);

        buttonsPanel.add(opButtonsPanel);
        buttonsPanel.add(filterButtonsPanel);

        panel.add(new JScrollPane(table), BorderLayout.CENTER);
        panel.add(buttonsPanel,BorderLayout.SOUTH);

        JFrame frame = new JFrame("Important message");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setContentPane(panel);
        frame.pack();
        frame.setVisible(true);

        TableRowSorter<CustomTableModel> sorterMy = new TableRowSorter<CustomTableModel>(t);



        bGetMinArtis.addActionListener(e->{
            table.setAutoCreateRowSorter(true);
            filterByArtist(sorterMy,t.getMinimumArtist());
            table.setRowSorter(sorterMy);
        });
        bGetMaxArtis.addActionListener(e-> {
            table.setAutoCreateRowSorter(true);
            table.setRowSorter(sorterMy);
            filterByArtist(sorterMy,t.getMaxiumumArtist());
        });
        bGetMinTitle.addActionListener(e->{
            table.setAutoCreateRowSorter(true);
            table.setRowSorter(sorterMy);
            filterByTitle(sorterMy,t.getMinimumTitle());
        });
        bGetMaxTitle.addActionListener(e-> {
            table.setAutoCreateRowSorter(true);
            table.setRowSorter(sorterMy);
            filterByTitle(sorterMy,t.getMaximumTitle());
        });
        bGetMinGenre.addActionListener(e->{
            table.setAutoCreateRowSorter(true);
            table.setRowSorter(sorterMy);
            filterByGenre(sorterMy,t.getMinimumGenre());
        });
        bGetMaxGenre.addActionListener(e-> {
            table.setAutoCreateRowSorter(true);
            table.setRowSorter(sorterMy);
            filterByGenre(sorterMy,t.getMaximumGenre());
        });
        bGetMinDate.addActionListener(e->{
            table.setAutoCreateRowSorter(true);
            table.setRowSorter(sorterMy);
            filterByDate(sorterMy,t.getMinimumDate());
        });
        bGetMaxDate.addActionListener(e-> {
            table.setAutoCreateRowSorter(true);
            table.setRowSorter(sorterMy);
            filterByDate(sorterMy,t.getMaximumDate());
        });
        bGetMinDuration.addActionListener(e->{
            table.setAutoCreateRowSorter(true);
            table.setRowSorter(sorterMy);
            filterByDuration(sorterMy,t.getMinimumDuration());
        });
        bGetMaxDuration.addActionListener(e-> {
            table.setAutoCreateRowSorter(true);
            table.setRowSorter(sorterMy);
            filterByDuration(sorterMy,t.getMaximumDuration());
        });

        s0.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                table.setRowSorter(sorterMy);
                rangeDuration(sorterMy, s0.getValue());
            }
        });

        s1.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                table.setRowSorter(sorterMy);
                minRangeDuration(sorterMy, s1.getValue());
            }
        });

        choseParameter.addItemListener(event -> {
            JComboBox comboBox = (JComboBox) event.getSource();

            Object item = event.getItem();


            if (event.getStateChange() == ItemEvent.SELECTED) {
                table.setRowSorter(sorterMy);
                textField.setText("");
                table.setAutoCreateRowSorter(true);
            }

        });

        t.addTableModelListener(new TableModelListener() {
            @Override
            public void tableChanged(TableModelEvent e) {
                table.setAutoCreateRowSorter(true);
                s1.setMinimum(t.getMinimumDuration());
                s1.setMaximum(t.getMaximumDuration());
                s0.setMinimum(t.getMinimumDuration());
                s0.setMaximum(t.getMaximumDuration());
                s0.setValue(t.getMaximumDuration());
                s1.setValue(t.getMinimumDuration());
            }
        });
        table.addMouseListener(new java.awt.event.MouseAdapter() {
                               @Override
                               public void mouseClicked(java.awt.event.MouseEvent evt) {
                                s0.setMaximum(t.getMaximumDuration());
                                s0.setMinimum(t.getMinimumDuration());
                                s0.setValue(t.getMaximumDuration());
                                s1.setMinimum(t.getMinimumDuration());
                                s1.setMaximum(t.getMaximumDuration());
                                s1.setValue(t.getMinimumDuration());
                               }
        });
        textField.addKeyListener(new KeyListener(){

            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyPressed(KeyEvent e) {

            }

            @Override
            public void keyReleased(KeyEvent e) {
                int searchParameter = (Integer)choseParameter.getSelectedIndex();
                String query = textField.getText();
                if(!query.isEmpty()){
                    table.setRowSorter(sorterMy);
                    switch (searchParameter){
                        case 5 : filterByDuration(sorterMy, Integer.valueOf(query)); break;
                        case 4 : filterByDate(sorterMy, Integer.valueOf(query)); break;
                        case 0 : sorterMy.setRowFilter(RowFilter.regexFilter(query)); break;
                        case 3 : filterByGenre(sorterMy,query); break;
                        case 2 : filterByArtist(sorterMy,query); break;
                        case 1 : filterByTitle(sorterMy,query); break;
                        default:break;
                    }

                }
                else{
                    table.setAutoCreateRowSorter(true);
                }
            }
        });



        bSearch.addActionListener(e->{
            TableRowSorter<CustomTableModel> s = new TableRowSorter<CustomTableModel>(t);
            table.setRowSorter(s);
            s.setRowFilter(RowFilter.regexFilter(textField.getText()));
        });

        bDelete.addActionListener(e->{
            int viewIndex = 0;
            viewIndex = table.getSelectedRow();
            if(viewIndex>=0){
                t.deleteRow(table.convertRowIndexToModel(viewIndex));
            }
        });

        bLoad.addActionListener(e -> {
            FileDialog fl = new FileDialog(frame,"Please chose",FileDialog.LOAD);
            fl.setVisible(true);
            String dir = fl.getDirectory();
            String file = fl.getFile();
            new SwingWorker<Void,Void>(){

            @Override
            protected Void doInBackground() throws Exception {
                t.load(dir,file);
                t.setRow(t.songList.size());
                t.setColumn(5);
                table.setAutoCreateRowSorter(true);
                return null;
            }

            @Override
            protected void done(){
                table.getColumnModel().getColumn(0).setMinWidth(MIN_WIDTH);
                t.updateMax(s0);
                s1.setMaximum(t.getMaximumDuration());
                s1.setMinimum(0);
                s1.setValue(0);

            }
        }.execute();
        frame.pack();
             }
        );

        bAdd.addActionListener(e ->
        {
            table.setAutoCreateRowSorter(true);
            s1.setValue(s1.getMinimum());
            s0.setValue(s0.getMaximum());
        });

        addNew.addActionListener(e -> {
            table.setAutoCreateRowSorter(true);
            t.addRow(new Song("", "", "", 0, 0));
            table.getColumnModel().getColumn(0).setMinWidth(MIN_WIDTH);
        });
        bSave.addActionListener(e -> {
            FileDialog fs = new FileDialog(frame,"Please chose",FileDialog.SAVE);
            fs.setVisible(true);

            if(fs.getFile() == null){
                System.out.println("uzytkownik nic nie wybral");
            }else {
                String dir = fs.getDirectory();
                String file = fs.getFile();
                try {
                    t.saveSongs(dir, file, t.songList);
                    JOptionPane.showMessageDialog(panel, SAVED+file);
                } catch (FileNotFoundException e1) {
                    e1.printStackTrace();
                }
            }
        });


    }


    void filterByDuration(final TableRowSorter sorter,int dur) {
        RowFilter<CustomTableModel, Integer> filterDur = new RowFilter<CustomTableModel, Integer>() {
            public boolean include(Entry<? extends CustomTableModel, ? extends Integer> entry) {
                CustomTableModel model = entry.getModel();
                Song song = model.songList.get(entry.getIdentifier());
                if (song.getDuration() ==dur) {
                    return true;
                }
                return false;
            }
        };
        sorter.setRowFilter(filterDur);
    }
    void rangeDuration(final TableRowSorter sorter,int dur) {
        RowFilter<CustomTableModel, Integer> rangeDur = new RowFilter<CustomTableModel, Integer>() {
            public boolean include(Entry<? extends CustomTableModel, ? extends Integer> entry) {
                CustomTableModel model = entry.getModel();
                Song song = model.songList.get(entry.getIdentifier());
                if (song.getDuration() <= dur) {
                    return true;
                }
                return false;
            }
        };
        sorter.setRowFilter(rangeDur);
    }

    void minRangeDuration(final TableRowSorter sorter,int dur) {
        RowFilter<CustomTableModel, Integer> rangeDur = new RowFilter<CustomTableModel, Integer>() {
            public boolean include(Entry<? extends CustomTableModel, ? extends Integer> entry) {
                CustomTableModel model = entry.getModel();
                Song song = model.songList.get(entry.getIdentifier());
                if (song.getDuration() >= dur) {
                    return true;
                }
                return false;
            }
        };
        sorter.setRowFilter(rangeDur);
    }

    void filterByDate(final TableRowSorter sorter, int date) {
        RowFilter<CustomTableModel, Integer> filterDate = new RowFilter<CustomTableModel, Integer>() {
            public boolean include(Entry<? extends CustomTableModel, ? extends Integer> entry) {
                CustomTableModel model = entry.getModel();
                Song song = model.songList.get(entry.getIdentifier());
                if (song.getDate() == date) {
                    return true;
                }
                return false;
            }
        };
        sorter.setRowFilter(filterDate);
    }

    void filterByArtist(final TableRowSorter sorter,String art) {
        RowFilter<CustomTableModel, Integer> filterArt = new RowFilter<CustomTableModel, Integer>() {
            public boolean include(Entry<? extends CustomTableModel, ? extends Integer> entry) {
                CustomTableModel model = entry.getModel();
                Song song = model.songList.get(entry.getIdentifier());
                if (song.getArtist().contains(art)) {
                    return true;
                }
                return false;
            }
        };
        sorter.setRowFilter(filterArt);
    }
    void filterByTitle(final TableRowSorter sorter,String tit) {
        RowFilter<CustomTableModel, Integer> filterTit = new RowFilter<CustomTableModel, Integer>() {
            public boolean include(Entry<? extends CustomTableModel, ? extends Integer> entry) {
                CustomTableModel model = entry.getModel();
                Song song = model.songList.get(entry.getIdentifier());
                if (song.getTitle().contains(tit)) {
                    return true;
                }
                return false;
            }
        };
        sorter.setRowFilter(filterTit);
    }
    void filterByGenre(final TableRowSorter sorter,String gen) {
        RowFilter<CustomTableModel, Integer> filterGen = new RowFilter<CustomTableModel, Integer>() {
            public boolean include(Entry<? extends CustomTableModel, ? extends Integer> entry) {
                CustomTableModel model = entry.getModel();
                Song song = model.songList.get(entry.getIdentifier());
                if (song.getGenre().contains(gen)) {
                    return true;
                }
                return false;
            }
        };
        sorter.setRowFilter(filterGen);
    }

    public static void main(String[] args) throws Exception {

        for(UIManager.LookAndFeelInfo lafi : UIManager.getInstalledLookAndFeels()) {
            System.out.println(lafi);
        }
        UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                new Playlist().startup();

            }
        });
    }
}
