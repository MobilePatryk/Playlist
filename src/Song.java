import java.util.Comparator;

public class Song implements Comparable<Song>{
    String title;
    String artist;
    String genre;
    int date;
    int duration;

    public Song(String t, String a, String g, int d, int du){
        this.title = t;
        this.artist = a;
        this.genre = g;
        this.date = d;
        this.duration = du;
    }
    public String toString(){
        return this.title + " " + this.artist + " " + this.genre + " " + this.date + " " + this.duration + "\n";
    }
    //Accessors
    public String getTitle(){
        return this.title;
    }

    public String getArtist() {
        return this.artist;
    }

    public String getGenre(){
        return this.genre;
    }

    public int getDate(){
        return this.date;
    }

    public int getDuration(){
        return this.duration;
    }

    @Override
    public int compareTo(Song o) {
        return title.compareTo(o.getTitle());
    } //tutaj mowie ze jezlei porownujesz 2 piosneki to porwonaj tylko tytul, zaimplementuj sprawdzenie innych warunkow.




}

class ArtistCompare implements Comparator<Song> {

    @Override
    public int compare(Song o1, Song o2) {
        return o1.getArtist().compareTo(o2.getArtist());
    }
}

class GenreCompare implements Comparator<Song> {

    @Override
    public int compare(Song o1, Song o2) {
        return o1.getGenre().compareTo(o2.getGenre());
    }
}

class DateCompare implements Comparator<Song>{

    @Override
    public int compare(Song o1, Song o2) {
        return Integer.compare(o1.date,o2.date);
    }
}

class DurationCompare implements Comparator<Song>{

    @Override
    public int compare(Song o1, Song o2) {
        return Integer.compare(o1.duration,o2.duration);
    }
}

//public static <T> T max(Collection<? extends T> coll,
//        Comparator<? super T> comp)
//Metoda dajaca max, min dzieki comparatorowi.