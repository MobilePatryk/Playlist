import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;


public class CustomTableModel extends AbstractTableModel {
    int row,column = 0;
    public ArrayList<Song> songList = new ArrayList<Song>();


    public CustomTableModel(int r, int c){
        row = r;
        column = c;
    }

    public void setRow(int r){
        row = r;
        fireTableStructureChanged();
    }

    public void setColumn(int c){
        column = c;
        fireTableStructureChanged();
    }

    @Override
    public int getRowCount() {
        return row;
    }

    @Override
    public int getColumnCount() {
        return column;
    }

    @Override
    public Object getValueAt(int r, int c) {
        switch (c){
            case 0 : return songList.get(r).getTitle();
            case 1 : return songList.get(r).getArtist();
            case 2 : return songList.get(r).getGenre();
            case 3 : return songList.get(r).getDate();
            case 4 : return songList.get(r).getDuration();
        }
        return null;
    }

//    public String secToMinute(int d){
//        int m = d/60;
//        int s = d%60;
//        if(d==0) return "";
//        else if(d<10) return "0:0"+d;
//        else if (s==0) {
//            return m + ":" + s + "0";
//        }
//        else return m + ":" + s;
//    }

//    @Override
//    public Class <?> getColumnClass(int col){
//        switch(col) {
//            case 0 : return String.class;
//            case 1 : return String.class;
//            case 2 : return String.class;
//            case 3 : return Integer.class;
//            case 4 : return Integer.class;
//            default: return Object.class;
//        }
//    }

    @Override
    public String getColumnName(int col){
        switch (col){
            case 1 : return "Autor";
            case 0 : return "Tytuł";
            case 2 : return "Gatunek";
            case 3 : return "Rok wydania";
            case 4 : return "Czas trwania";
            default : return null;
        }

    }

    @Override
    public boolean isCellEditable(int row,int col){
        return true;
    }

    @Override
    public void setValueAt(Object value, int row, int col){

            switch(col){
                case 0 : songList.get(row).title = (String)value;
                break;
                case 1 : songList.get(row).artist = (String)value;
                break;
                case 2 : songList.get(row).genre = (String)value;
                break;
                case 3 : songList.get(row).date = Integer.valueOf((String)value);
                break;
                case 4 : songList.get(row).duration = Integer.valueOf((String)value);
                break;
            }
    }

    public void deleteRow(int ro){
        if(ro>=0 && ro<songList.size()){
            songList.remove(ro);
            setRow(row-1);
            fireTableRowsDeleted(ro,ro);
        }
    }

    public void addRow(Song song){
        int newRowIndex = songList.size();
        songList.add(song);
        setRow(newRowIndex+1);
//        Musialem tutaj umiescic akcesor zwiekszajacy o 1, inaczej wyrzuca
//        invalid range exceptions -> po sciezce bledu wywnioskowalem ze
//        wyjatek jest rzucamny wtedy gdy:
//        public void rowsInserted(int firstRow, int endRow) {
//            checkAgainstModel(firstRow, endRow);
//            int newModelRowCount = getModelWrapper().getRowCount();
//            if (endRow >= newModelRowCount) {
//                throw new IndexOutOfBoundsException("Invalid range");
//            }
//            modelRowCount = newModelRowCount;
//            if (shouldOptimizeChange(firstRow, endRow)) {
//                rowsInserted0(firstRow, endRow);
//            }
//        }
        fireTableRowsInserted(newRowIndex,newRowIndex);
    }



    public void load(String dir,String fileN) throws InterruptedException {
        songList.clear();
        Thread.sleep(1000);
        getSongs(dir,fileN);
        //fillTheData();
        System.out.println(songList);
    }
    public void updateMax(JSlider slider){
        slider.setMaximum(this.getMaximumDuration());
    }
    public void updateMin(JSlider slider){
        slider.setMinimum(this.getMinimumDuration());
    }
    void addSong(String lineToParse){
        String[] tokens = lineToParse.split("/");
        try {
            Song nextSong = new Song(tokens[0], tokens[1], tokens[2], Integer.parseInt(tokens[3]), Integer.parseInt(tokens[4]));
            songList.add(nextSong);
        }
        catch(NumberFormatException e){
            System.out.println("Zly typ danych dla " + Arrays.toString(tokens));
        }
        //JTextfield -> getDocument -> DocumentFilter
    }

    void getSongs(String dir,String fileN){
        try{
            File file = new File(dir,fileN);
            BufferedReader reader = new BufferedReader(new FileReader(file));
            String line = null;
            while((line=reader.readLine()) != null){
                addSong(line);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void minGivenDuration(int dur){
        for(Song num : songList){
            if(num.getDuration()>= dur){
                System.out.println(num);
            }
        }
    }
    void maxGivenDuration(int dur){
        for(Song num : songList){
            if(num.getDuration() <= dur){
                System.out.println(num);
            }
        }
    }
    void searchByArtist(String a){
        for(Song num : songList){
            if(num.getArtist().contains(a)){
                System.out.println(num);
            }
        }
    }

    int getMaximumDuration(){
        int max = songList.get(0).getDuration();
        for(Song x : songList){
            if(x.getDuration()>max){
                max = x.getDuration();
            }
        }
        return max;
    }
    int getMinimumDuration(){
        int max = songList.get(0).getDuration();
        for(Song x : songList){
            if(x.getDuration()<max){
                max = x.getDuration();
            }
        }
        return max;
    }
    int getMinimumDate(){
        int max = songList.get(0).getDate();
        for(Song x : songList){
            if(x.getDate()<max){
                max = x.getDate();
            }
        }
        return max;
    }
    int getMaximumDate(){
        int max = songList.get(0).getDate();
        for(Song x : songList){
            if(x.getDate()>max){
                max = x.getDate();
            }
        }
        return max;
    }

    String getMaxiumumArtist(){
        String max = songList.get(0).getArtist();
        for(Song x : songList){
            if(x.getArtist().compareTo(max) > 0){
                max = x.getArtist();
            }
        }
        return max;
    }

    String getMinimumArtist(){
        String max = songList.get(0).getArtist();
        for(Song x : songList){
            if(x.getArtist().compareTo(max) < 0){
                max = x.getArtist();
            }
        }
        return max;
    }

    String getMinimumTitle(){
        String max = songList.get(0).getTitle();
        for(Song x : songList){
            if(x.getTitle().compareTo(max) < 0){
                max = x.getTitle();
            }
        }
        return max;
    }
    String getMaximumTitle(){
        String max = songList.get(0).getTitle();
        for(Song x : songList){
            if(x.getTitle().compareTo(max) > 0){
                max = x.getTitle();
            }
        }
        return max;
    }

    String getMinimumGenre(){
        String max = songList.get(0).getGenre();
        for(Song x : songList){
            if(x.getGenre().compareTo(max) < 0){
                max = x.getGenre();
            }
        }
        return max;
    }

    String getMaximumGenre(){
        String max = songList.get(0).getGenre();
        for(Song x : songList){
            if(x.getGenre().compareTo(max) > 0){
                max = x.getGenre();
            }
        }
        return max;
    }





    void saveSongs(String dir,String name, ArrayList<Song> p) throws FileNotFoundException {
        File file = new File(dir,name);
        PrintWriter pw = new PrintWriter(new FileOutputStream(file));
        for(int i = 0; i<p.size(); i++){
            pw.print(p.get(i).getTitle());
            pw.print("/");
            pw.print(p.get(i).getArtist());
            pw.print("/");
            pw.print(p.get(i).getGenre());
            pw.print("/");
            pw.print(p.get(i).getDate());
            pw.print("/");
            pw.print(p.get(i).getDuration());
            pw.print("\n");
        }
        pw.close();

    }
}

